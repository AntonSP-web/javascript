/**
 *
 * @param data: {String, Number, Object}
 * @returns {String, Number, Object}
 */
module.exports.deafPhoneGame = function deafPhoneGame(data) {
    // implement your solution here
    throw new Error('Not implemented');
};

/**
 *
 * @param player1: {String}
 * @param score: {String}
 * @param player2: {String}
 * @returns {String}
 */
module.exports.nameHockeyGameWinner = function nameHockeyGameWinner(player1, score, player2) {
    // implement your solution here
    throw new Error('Not implemented');
};

/**
 *
 * @param arr: {Array}
 * @param firstElementIndex: {Number}
 * @param secondElementIndex: {Number}
 * @returns {Array}
 */
module.exports.swapTwoElementsInArray = function swapTwoElementsInArray(arr, firstElementIndex, secondElementIndex) {
    // implement your solution here
    throw new Error('Not implemented');
};

/**
 *
 * @param str: {String}
 * @returns {Boolean}
 */
module.exports.isBracketsBalanced = function isBracketsBalanced(str) {
    // implement your solution here
    throw new Error('Not implemented');
};

/**
 *
 * @param x: {Object, Array}
 * @param y: {Object, Array}
 * @returns {Boolean}
 */
module.exports.deepEqual = function deepEqual(x, y) {
    // implement your solution here
    throw new Error('Not implemented');
};
