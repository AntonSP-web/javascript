const assert = require('assert');

const {
    deafPhoneGame,
    nameHockeyGameWinner,
    swapTwoElementsInArray,
    isBracketsBalanced,
    deepEqual,
} = require('../src');

describe('deafPhoneGame', () => {
    it('Should return "hi" for given input "hi"', () => {
        const value = deafPhoneGame('hi');

        assert.equal(value, 'hi');
    });

    it('Should return 123 for given input 123', () => {
        const value = deafPhoneGame(123);

        assert.equal(value, 123);
    });

    it('Should return {name: "Name"} for given input {name: "Name"}', () => {
        const value = deafPhoneGame({ name: 'Name' });

        assert.deepEqual(value, { name: 'Name' });
    });
});

describe('nameHockeyGameWinner', () => {
    it('Should return "Bolts defeat Capitals with the score 5:4" for given input', () => {
        const value = nameHockeyGameWinner('Bolts', '5:4', 'Capitals');

        assert.equal(value, 'Bolts defeat Capitals with the score 5:4');
    });

    it('Should return "Bruins defeat Red Wings with the score 4:2" for given input', () => {
        const value = nameHockeyGameWinner('Red Wings', '2:4', 'Bruins');

        assert.equal(value, 'Bruins defeat Red Wings with the score 4:2');
    });

    it('Should return "Flames defeat Stars in OT with the score 7:6OT" for given input', () => {
        const value = nameHockeyGameWinner('Stars', '6:7OT', 'Flames');

        assert.equal(value, 'Flames defeat Stars in OT with the score 7:6OT');
    });
});

describe('swapTwoElementsInArray', () => {
    it('Should return "[3, 12, 65]" for given input', () => {
        const value = swapTwoElementsInArray([65, 12, 3], 0, 2);

        assert.deepEqual(value, [3, 12, 65]);
    });

    it('Should return "[3, 4, -1, 12, 65]" for given input', () => {
        const value = swapTwoElementsInArray([3, 4, 65, 12, -1], 2, 4);

        assert.deepEqual(value, [3, 4, -1, 12, 65]);
    });

    it('Should return "[3, 4, 65, 12, -1]" for given input', () => {
        const value = swapTwoElementsInArray([3, 4, 65, 12, -1], -100, 4);

        assert.deepEqual(value, [3, 4, 65, 12, -1]);
    });
});

describe('isBracketsBalanced', () => {
    it('Should return true for given input', () => {
        const value = isBracketsBalanced('[[][]]');

        assert.equal(value, true);
    });

    it('Should return true for given input', () => {
        const value = isBracketsBalanced('[{}]');

        assert.equal(value, true);
    });

    it('Should return false for given input', () => {
        const value = isBracketsBalanced('({}[]<>(((())))');

        assert.equal(value, false);
    });
});

describe('deepEqual', () => {
    it('Should return true for given input', () => {
        const obj1 = {a: 1, b: 2};
        const obj2 = {a: 1, b: 2};
        assert.equal(deepEqual(obj1, obj2), true);
    });

    it('Should return false for given input', () => {
        const obj1 = {a: 1, b: 2};
        const obj2 = {a: 4, b: 5};
        assert.equal(deepEqual(obj1, obj2), false);
    });

    it('Should return true for given input', () => {
        const obj1 = {a: 1, b: 2};
        assert.equal(deepEqual(obj1, obj1), true);
    });

    it('Should return false for given input', () => {
        const arr1 = [1, 2, 3];
        const arr2 = [3, 2, 1];
        assert.equal(deepEqual(arr1, arr2), false);
    });

    it('Should return true for given input', () => {
        const arr1 = [1, 2, 3];
        const arr2 = [1, 2, 3];
        assert.equal(deepEqual(arr1, arr2), true);
    });
});
