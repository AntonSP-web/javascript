# Javascript PreScreening

## Tasks

### Deaf Phone Game

You're playing a game called Deaf Phone.
Your task is to get the data which computer tells you and reproduce it completely. 
For example:
```js
deafPhoneGame('Ouagadougou is the capital of Burkina Faso'); // returns 'Ouagadougou is the capital of Burkina Faso'
deafPhoneGame({ topSecretHash: 'f2jjJ()F@#()d@', topSecretNumber: -312 }); // returns { topSecretHash: 'f2jjJ()F@#()d@', topSecretNumber: -312 }
```

### Name Hockey Game Winner

Your tasks is to create a title for a some News Portal with an info about the result of the latest hockey match.
You function received three params: one team name, final score and another one team name.
You should return a string containing the match result in a special format: `%Winner_team_name% defeat %Loser_team_name% %'in OT with the score' OR 'with the score'% %score%`
For example:
```js
nameHockeyGameWinner('Bolts', '5:4', 'Capitals'); // returns 'Bolts defeat Capitals with the score 5:4'
nameHockeyGameWinner('Stars', '6:7OT', 'Flames'); // returns 'Flames defeat Stars in OT with the score 7:6OT'
```

### Swap Two Elements in an Array
Your task is to swap two elements in a provided array and elements indexes.
If it's not possible, return the given array. For example
```js
swapTwoElementsInArray([65, 12, 3], 0, 2); // returns [3, 12, 65]
swapTwoElementsInArray([3, 4, 65, 12, -1], -100, 4); // returns [3, 4, 65, 12, -1]
```

### Balanced Brackets
Your tasks is to implement a function which return true if the specified string has the balanced brackets and false otherwise.
Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
(in that order), none of which mis-nest.
Brackets include [],(),{},<>
For example
```js
isBracketsBalanced('[[][]]'); // return true
isBracketsBalanced('({}[]<>(((())))'); // return false
```

### Deep equal
Your tasks is to create a equality checker function. It should works at least for objects and arrays.
For example
```js
const obj1 = {a: 1, b: 2};
const obj2 = {a: 1, b: 2};
deepEqual(obj1, obj2); // true

const arr1 = [1, 2, 3];
const arr2 = [3, 2, 1];
deepEqual(arr1, arr2); // false
```

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)
2. Fork this repository: Javascript
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript/  
4. Go to folder `javascript`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests

## Submit to [AutoCode](https://autocode.lab.epam.com/)
1. Open [AutoCode](https://autocode.lab.epam.com/) and login
2. Subscribe to [JavaScript Mentoring | JavaScript PreScreening](https://autocode.lab.epam.com/student/group/42)
3. Select your task (JavaScript PreScreening Tasks)
4. Submit your solution to Autocode using `Check task` button

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
